from django.apps import AppConfig

from django.utils.translation import gettext_lazy as _


class SBFileConfig(AppConfig):
    name = 'sb_file'
    verbose_name = _('File')
