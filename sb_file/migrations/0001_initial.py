from django.db import models, migrations
import sb_core.folder_mechanism
import sb_core.cms_models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0013_urlconfrevision'),
    ]

    operations = [
        migrations.CreateModel(
            name='SBFile',
            fields=[
                ('cmsplugin_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE)),
                ('folder', models.CharField(max_length=5, null=True, editable=False, blank=True)),
                ('file', models.FileField(upload_to=sb_core.folder_mechanism.upload_to_handler, max_length=255, verbose_name='\u0424\u0430\u0439\u043b')),
                ('title', models.CharField(max_length=255, null=True, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True)),
                ('hint', models.CharField(help_text='\u041f\u043e\u044f\u0432\u043b\u044f\u0435\u0442\u0441\u044f \u043f\u0440\u0438 \u043d\u0430\u0432\u0435\u0434\u0435\u043d\u0438\u0438 \u043c\u044b\u0448\u043a\u043e\u0439 \u043d\u0430 \u043e\u0431\u044a\u0435\u043a\u0442', max_length=255, null=True, verbose_name='\u041f\u043e\u0434\u0441\u043a\u0430\u0437\u043a\u0430', blank=True)),
                ('is_show_icon', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0438\u043a\u043e\u043d\u043a\u0443 \u0444\u0430\u0439\u043b\u0430?')),
                ('is_show_file_size', models.BooleanField(default=True, verbose_name='\u041f\u043e\u043a\u0430\u0437\u044b\u0432\u0430\u0442\u044c \u0440\u0430\u0437\u043c\u0435\u0440 \u0444\u0430\u0439\u043b\u0430?')),
            ],
            options={
                'db_table': 'sb_file',
                'verbose_name': '\u0424\u0430\u0439\u043b',
                'verbose_name_plural': '\u0424\u0430\u0439\u043b\u044b',
            },
            bases=(sb_core.cms_models.SBCMSPluginMixin, 'cms.cmsplugin', models.Model),
        ),
    ]
