from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_file', '0002_auto_20160716_1211'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbfile',
            name='folder',
            field=models.CharField(default='', max_length=5, editable=False, blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sbfile',
            name='hint',
            field=models.CharField(default='', help_text='\u041f\u043e\u044f\u0432\u043b\u044f\u0435\u0442\u0441\u044f \u043f\u0440\u0438 \u043d\u0430\u0432\u0435\u0434\u0435\u043d\u0438\u0438 \u043c\u044b\u0448\u043a\u043e\u0439 \u043d\u0430 \u043e\u0431\u044a\u0435\u043a\u0442', max_length=255, verbose_name='\u041f\u043e\u0434\u0441\u043a\u0430\u0437\u043a\u0430', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='sbfile',
            name='title',
            field=models.CharField(default='', max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435', blank=True),
            preserve_default=False,
        ),
    ]
