from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sb_file', '0003_auto_20160809_1301'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbfile',
            name='cmsplugin_ptr',
            field=models.OneToOneField(parent_link=True, related_name='sb_file_sbfile', auto_created=True, primary_key=True, serialize=False, to='cms.CMSPlugin', on_delete=models.CASCADE),
        ),
    ]
