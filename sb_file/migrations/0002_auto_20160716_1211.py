from django.db import models, migrations
import sb_core.folder_mechanism
import sb_core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sb_file', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sbfile',
            name='file',
            field=sb_core.fields.FileField(upload_to=sb_core.folder_mechanism.upload_to_handler, max_length=255, verbose_name='\u0424\u0430\u0439\u043b'),
        ),
    ]
