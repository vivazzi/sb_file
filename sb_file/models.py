from os.path import basename, splitext
from django.db import models
from django.utils.translation import gettext_lazy as _

from sb_core.constants import hint_ht
from sb_core.fields import FileField
from sb_core.cms_models import FileModelPlugin


class SBFile(FileModelPlugin):
    file = FileField(_('File'))
    title = models.CharField(_('Title'), max_length=255, blank=True)
    hint = models.CharField(_('Hint'), max_length=255, blank=True, help_text=hint_ht)

    is_show_ext = models.BooleanField(_('Show file extension?'), default=False)
    is_show_icon = models.BooleanField(_('Show file icon?'), default=True)
    is_show_file_size = models.BooleanField(_('Show file size?'), default=True)

    rel = models.BooleanField(
        _('Allow search engines to follow the link?'), default=False,
        help_text=_('Usually not required to select, which allows you to not reduce the position of the site in search '
                    'engines. <br/> Select if you want to allow robots to follow the link.<br/>'
                    'More details on the page: <a rel="nofollow" target="_blank" '
                    'href="http://vits.pro/info/site-positions/">Site positions in search engines </a>'))

    # parent_instance need for download counter
    parent_instance_id = models.PositiveIntegerField(null=True, blank=True, editable=False)

    def copy_relations(self, old_instance):
        super(SBFile, self).copy_relations(old_instance)

        self.parent_instance_id = old_instance.id
        self.save()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.title:
            self.title = splitext(self.file.name)[0]
        super(SBFile, self).save()

    def get_icon_name(self):
        ext = self.get_ext()

        # if extension like icon
        if ext in ('jpg', 'pcx', 'tga', 'bmp', 'gif'): return ext

        # other cases
        if ext in ('psd', ''): return 'psd'
        if ext in ('doc', 'docx'): return 'doc'
        if ext in ('xls', 'xlsx'): return 'xls'
        if ext in ('ppt', 'pptx'): return 'ppt'
        if ext in ('pdf', 'djvu', 'epub'): return 'pdf'
        if ext in ('mov', 'qt'): return 'quick_time'
        if ext in ('zip', 'rar', 'tar', 'gz', 'jar'): return 'zip'
        if ext in ('bmp', 'jpeg', 'png', 'ico'): return 'image'
        if ext in ('mp3', 'wav'): return 'audio'
        if ext in ('avi', 'mp4', 'mts'): return 'video'

        return 'txt'
    
    def get_filename(self):
        return basename(self.file.name)
        
    def get_ext(self):
        return splitext(self.get_filename())[1][1:].lower()

    def get_title(self):
        if not self.file:
            return _('<empty>')

        title = self.title
        if self.is_show_ext:
            title = f'{title}{splitext(self.file.name)[1]}'

        return title

    def __str__(self):
        return self.get_title()

    class Meta:
        db_table = 'sb_file'
        verbose_name = _('File')
        verbose_name_plural = _('Files')
