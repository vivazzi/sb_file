from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase

from sb_core.constants import BASE
from sb_file.models import SBFile


class SBFilePlugin(CMSPluginBase):
    module = BASE
    model = SBFile
    name = 'Файл'
    render_template = 'sb_file/sb_file.html'
    text_enabled = True

    search_fields = ('title', 'link_title')


plugin_pool.register_plugin(SBFilePlugin)
