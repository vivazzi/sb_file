��          |      �             !  (   )     R     W     d     j     o     �     �     �  9  �  �  �     �  ^   �          &  
   >     I  5   \  -   �  -   �     �  �  �                                         	   
               <empty> Allow search engines to follow the link? File File is lost Files Hint Show file extension? Show file icon? Show file size? Title Usually not required to select, which allows you to not reduce the position of the site in search engines. <br/> Select if you want to allow robots to follow the link.<br/>More details on the page: <a rel="nofollow" target="_blank" href="http://vits.pro/info/site-positions/">Site positions in search engines </a> Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
 <пусто> Позволить поисковым системам переходить по ссылке? Файл Файл потерян Файлы Подсказка Показывать расширение файла? Показывать иконку файла? Показывать размер файла? Название Обычно не требуется отмечать, что позволяет не снижать позиции сайта в поисковых системах.<br/>Отметьте, если требуется позволить роботам переходить по ссылке.<br/>Подробнее на странице:<a rel="nofollow" target="_blank" href="http://vits.pro/info/site-positions/">Позиции сайта в поисковых системах</a> 