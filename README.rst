=======
sb_file
=======

sb_file is django-cms plugin which is part of SBL (Service bussiness library, https://vits.pro/dev/).

This plugin adds file to page.


Installation
============

sb_file requires sb_core package: https://bitbucket.org/vivazzi/sb_core/

There is no sb_file in PyPI, so you can install this package from bitbucket repository only.

::
 
     $ pip install hg+https://bitbucket.org/vivazzi/sb_file


Configuration 
=============

1. Add "sb_file" to INSTALLED_APPS after "sb_core" ::

    INSTALLED_APPS = (
        ...
        'sb_core',
        'sb_file',
        ...
    )

2. Run `python manage.py migrate` to create the sb_file models.  
